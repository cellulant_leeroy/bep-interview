bep-interview - README File

Installation :

{PLATFORM} can either be win32 or linux, depending on the base operating system you are using.

- Unzip the install file
- move the bep-interview directory to the location of your choice, recommend a separate drive in \srv\
- open bep-interview\{PLATFORM}\bin
- execute the following command in a cmd / shell

WIN32:
- bep-interview.exe --install ../etc/wrapper.conf

LINUX:
- bep-interview --install ../etc/wrapper.conf

- You may now execute the service
 - Win32 - Service - bep-interview start
 - Linux - Init.d - /etc/init.d/bep-interview start

