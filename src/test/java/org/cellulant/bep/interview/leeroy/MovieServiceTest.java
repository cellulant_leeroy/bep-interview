package org.cellulant.bep.interview.leeroy;


import org.cellulant.bep.interview.leeroy.business.api.MovieService;
import org.cellulant.bep.interview.leeroy.business.dto.MovieDto;
import org.cellulant.bep.interview.leeroy.business.impl.MovieServiceImpl;
import org.cellulant.bep.interview.leeroy.dal.model.Movie;
import org.cellulant.bep.interview.leeroy.dal.repository.MovieRepository;
import org.cellulant.bep.interview.leeroy.util.RestEmissary;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.sugarize.JsonUtil;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.powermock.api.mockito.PowerMockito.spy;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RestEmissary.class, JsonUtil.class})
public class MovieServiceTest {

    @InjectMocks
    private MovieServiceImpl movieService = new MovieServiceImpl();
    @Mock
    private MovieRepository movieRepository;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(movieService, "assessmentUrl", "https://beep2.cellulant.com:9001/assessment");
        ReflectionTestUtils.setField(movieService, "movieRepository", movieRepository);
    }

    @Test
    public void testFetchFromRemoteCallsRestEmissary() {
        movieService.getMoviesFromRemote();

        PowerMockito.verifyStatic(RestEmissary.class, times(1));
    }

    @Test
    public void testFetchFromRemoteCallsJsonUtil() {
        movieService.getMoviesFromRemote();

        PowerMockito.verifyStatic(JsonUtil.class, times(1));
    }

    @Test
    public void testThatWriteMoviesToDatabaseCallsRepository(){
        List<MovieDto> movieDtos = new LinkedList<>();

        MovieDto movieDto = new MovieDto();
        movieDto.setGenre("dummyGenre");
        movieDto.setMovieId("dummyMovieId");
        movieDto.setTitle("dummyTitle");

        movieDtos.add(movieDto);

        movieService.writeMoviesToDatabase(movieDtos);

        Mockito.verify(movieRepository, times(1))
                .save(ArgumentMatchers.any(Movie.class));
    }

    @Test
    public void testInitMethod(){

        MovieServiceImpl mock = spy(movieService);
        mock.init();

        Mockito.verify(mock, times(1))
                .getMoviesFromRemote();

        Mockito.verify(mock, times(1))
                .writeMoviesToDatabase(anyList());
    }
}
