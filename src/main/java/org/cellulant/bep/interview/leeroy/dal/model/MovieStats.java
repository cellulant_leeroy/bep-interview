package org.cellulant.bep.interview.leeroy.dal.model;

import org.springframework.sugarize.jpa.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "movie_stats")
public class MovieStats extends AbstractEntity {
    private String genre;
    @Column(name = "number_of_movies")
    private Integer numberOfMovies;

    protected void afterFetch() {

    }

    protected void beforeUpdate() {

    }

    protected void beforeInsert() {

    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getNumberOfMovies() {
        return numberOfMovies;
    }

    public void setNumberOfMovies(Integer numberOfMovies) {
        this.numberOfMovies = numberOfMovies;
    }
}
