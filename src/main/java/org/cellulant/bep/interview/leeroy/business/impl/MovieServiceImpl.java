package org.cellulant.bep.interview.leeroy.business.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import org.cellulant.bep.interview.leeroy.business.api.MovieService;
import org.cellulant.bep.interview.leeroy.business.dto.MovieDto;
import org.cellulant.bep.interview.leeroy.dal.model.Movie;
import org.cellulant.bep.interview.leeroy.dal.model.MovieStats;
import org.cellulant.bep.interview.leeroy.dal.repository.MovieRepository;
import org.cellulant.bep.interview.leeroy.dal.repository.MovieStatsRepository;
import org.cellulant.bep.interview.leeroy.util.RestEmissary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

import static org.cellulant.bep.interview.leeroy.config.AppConfig.dozerMapper;
import static org.springframework.sugarize.JsonUtil.jsonParse;

@Component
public class MovieServiceImpl implements MovieService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${beep2.cellullant.url:https://beep2.cellulant.com:9001/assessment}")
    private String assessmentUrl;

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieStatsRepository movieStatsRepository;

    @PostConstruct
    public void init() {
        writeMoviesToDatabase(getMoviesFromRemote());
    }

    public List<MovieDto> getMoviesFromRemote() {
        try {
            return jsonParse(RestEmissary.getFromUrl(assessmentUrl), new TypeReference<List<MovieDto>>() {
            });
        } catch (IOException e) {
            logger.warn("{}", e);
            return new LinkedList<>();
        }
    }

    public void writeMoviesToDatabase(final List<MovieDto> movies) {
        movies.stream()
                .map(movieDto -> dozerMapper().map(movieDto, Movie.class))
                .forEach(movie -> {
                    movieRepository.save(movie);
                });

        generateStats(movies);
    }

    public void generateStats(List<MovieDto> movieDtos) {
        List<MovieStats> movieStats = new LinkedList<>();
        List<String> genres = new LinkedList<>();

        Map<String, Integer> genreStats = new LinkedHashMap<>();

        movieDtos.stream()
                .forEach(movieDto -> {
                    Arrays.asList(movieDto.getGenre().split("\\|"))
                            .stream()
                            .forEach(genre -> {
                                if (!genres.contains(genre)) {
                                    genres.add(genre);
                                }
                            });
                });

        movieDtos.stream()
                .forEach(movieDto -> {
                    Arrays.asList(movieDto.getGenre().split("\\|"))
                            .stream()
                            .forEach(genre -> incrementGenre(genreStats, genre));
                });

        genreStats.entrySet().stream()
                .forEach(stringIntegerEntry -> {
                    MovieStats movieStats1 = new MovieStats();
                    movieStats1.setGenre(stringIntegerEntry.getKey());
                    movieStats1.setNumberOfMovies(stringIntegerEntry.getValue());
                    movieStats.add(movieStats1);
                });

        movieStats.stream()
                .forEach(movieStats1 -> {
                    movieStatsRepository.save(movieStats1);
                });
    }

    public void incrementGenre(Map<String, Integer> genreStats, String genre) {
        Integer value = genreStats.get(genre);

        if (value == null) {
            value = 0;
        } else {
            value = value + 1;
        }

        genreStats.remove(genre);
        genreStats.put(genre, value);
    }
}

