package org.cellulant.bep.interview.leeroy.dal.model;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.sugarize.jpa.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "movies")
public class Movie extends AbstractEntity {
    @Column(name = "movie_id")
    private String movieId;
    private String title;
    private String genre;

    public void afterFetch() {

    }

    public void beforeUpdate() {

    }

    public void beforeInsert() {

    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
