package org.cellulant.bep.interview.leeroy.dal.repository;

import org.cellulant.bep.interview.leeroy.dal.model.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.sugarize.jpa.AbstractJpaRepository;

public interface MovieRepository extends AbstractJpaRepository<Movie> {
}
