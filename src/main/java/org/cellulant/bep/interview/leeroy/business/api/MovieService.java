package org.cellulant.bep.interview.leeroy.business.api;

import org.cellulant.bep.interview.leeroy.business.dto.MovieDto;

import java.util.List;

public interface MovieService {

    List<MovieDto> getMoviesFromRemote();

    void writeMoviesToDatabase(List<MovieDto> movies);
}
