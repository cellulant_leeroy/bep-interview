package org.cellulant.bep.interview.leeroy.config;

import org.cellulant.bep.interview.leeroy.business.LogicMarker;
import org.cellulant.bep.interview.leeroy.dal.model.EntityMarker;
import org.cellulant.bep.interview.leeroy.dal.repository.RepositoryMarker;
import org.dozer.DozerBeanMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Collections;

@Configuration
@EntityScan(basePackageClasses = {EntityMarker.class})
@EnableJpaRepositories(basePackageClasses = {RepositoryMarker.class})
@ComponentScan(basePackageClasses = {LogicMarker.class})
public class AppConfig {
    private static DozerBeanMapper dozerInstance;

    public static DozerBeanMapper dozerMapper() {
        if (dozerInstance == null) {
            dozerInstance = new DozerBeanMapper();
            dozerInstance.setMappingFiles(Collections.singletonList("dozerJdk8Converters.xml"));
        }
        return dozerInstance;
    }
}
