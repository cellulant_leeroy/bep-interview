package org.cellulant.bep.interview.leeroy.dal.repository;

import org.cellulant.bep.interview.leeroy.dal.model.MovieStats;
import org.springframework.sugarize.jpa.AbstractJpaRepository;

public interface MovieStatsRepository extends AbstractJpaRepository<MovieStats> {
}
